# TODO


## Games

##### Kirby
- [x] Gourmet Race
- [x] Green Greens

##### Trine
- [x] Trine 2 Main Theme

##### Legend of Zelda
- [ ] Windwaker Introduction
- [ ] Outset Island
- [ ] The Great Sea
- [x] Lost Woods
- [x] Ordon Village

##### Mario Kart
- [x] Rainbow Road (Double Dash)
- [ ] Sherbet Land (Double Dash)
- [ ] Mushroom City (Double Dash)

##### Portal
- [ ] Still Alive

##### Street Fighter
- [x] Guile's Theme

##### Castlevania
- [ ] Stage 1 (NES)

##### Duck Tales
- [x] Moon Theme

##### Super Smash Brothers Melee
- [ ] Main Menu

##### Super Mario Galaxy
- [ ] Observatory Theme

##### Skyrim
- [ ] Skyrim Main Theme

##### Angry Birds
- [ ] Angry Birds Title Theme

##### Tetris
- [ ] Tetris Theme A
- [ ] Tetris Theme B

##### Yoshi's Island
- [ ] Flower Garden
- [x] Athletic Theme

##### Pokemon
- [x] Wild Pokemon Battle (Ruby and Sapphire)



## Popular Music

##### Enya
- [x] Only Time

##### Celine Dion
- [x] That's the Way It Is
- [x] My Heart Will Go On

##### Simon and Garfunkel
- [ ] Scarborough Fair
- [ ] The Sounds of Silence

##### John Denver
- [ ] Annie's Song

##### Billy Joel
- [x] The Longest Time



## Electronic
- [ ] ISM - Savant
- [x] Nyan Cat


## Neo Classical

- [ ] Pachelbel's Canon
- [ ] Ave Maria
- [ ] Entry of the Gladiators



## Movies and TV

##### Game of Thrones
- [ ] Game of Thrones Main Theme

##### The Greatest American Hero
- [ ] Believe It or Not

##### Aladdin
- [x] A Whole New World

##### Lion King
- [ ] Circle of Life

##### Mulan
- [ ] I'll Make a Man Out of You

##### Charlie Brown
- [ ] Linus and Lucy

##### Lord of the Rings
- [ ] Concerning Hobbits

##### Jurassic Park

##### Pink Panther
- [ ] Pink Panther Theme

##### A-Team
- [ ] A-Team Main Theme

##### The Sound of Music
- [x] Edelweiss

##### Pirates of the Caribbean

##### Star Wars

